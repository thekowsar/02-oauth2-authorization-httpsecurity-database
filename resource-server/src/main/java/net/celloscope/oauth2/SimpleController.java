package net.celloscope.oauth2;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class SimpleController {

    @GetMapping("/whoami")
    public String whoami(@AuthenticationPrincipal Principal principal) {
        System.out.println(principal.getName());
        return principal.getName();
    }

    @RequestMapping("/test")
    public String test() {
        return "Hello World";
    }
    
    @RequestMapping("/all")
    public String permitAll() {
        return "Hello for all users.";
    }

}
